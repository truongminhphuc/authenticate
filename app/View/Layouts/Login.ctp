<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$cakeDescription=__d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion=__d('cake_dev', 'CakePHP %s',Configure::version());
?>
<!DOCTYPE html>
<html>
    <head>
        <?php echo $this-Html->charset(); ?>
        <title>
            <?php echo $cakeDescription ?>:
           <?php echo $this->fetch('title'); ?>
        </title>
        <?php
            echo $this->Html->css('login/bootstrap.min');
		echo $this->Html->css('login/style');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
        ?>
    </head>
    <body>
        <div id="container">

		<div id="content">

			<?php echo $this->Flash->render(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>

	</div>
	<?php echo $this->element('sql_dump'); ?>
    </body>
</html>

